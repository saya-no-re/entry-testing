function fib(n) {
  var [a, b] = [1, 1];
  while (n-- > 1) {
    [a, b] = [b, a + b];
  }
  return b;
}
